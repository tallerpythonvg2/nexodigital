from django import forms
from .models import Estudiante

class EstudianteForm(forms.Form):
    nombre = forms.CharField(max_length=100)
    nomCarrera = forms.CharField(max_length=100)
    semestre = forms.IntegerField()

class EstudianteModelForm(forms.ModelForm):
    class Meta:
        modelo = Estudiante
        campos = ["nombre", "edad"]