from django.db import models

# Create your models here

class Estudiante(models.Model):

    nombre = models.CharField(max_length=100)
    edad = models.IntegerField()
    rut = models.IntegerField()
    carrera = models.CharField(max_length=100)
    fechaIngreso = models.DateTimeField(auto_now_add=True, auto_now=False)

    def __str__(self):
        return self.nombre
