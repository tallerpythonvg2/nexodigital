from django.contrib import admin
# Register your models here.
from estudiantes.models import *

class AdminEstudiante(admin.ModelAdmin):
    list_display = ["nombre", "edad"]
    #form = RegModelForm
    list_filter = ["nombre", "edad"]

admin.site.register(Estudiante, AdminEstudiante)