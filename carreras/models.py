from django.db import models

# Create your models here.

class Carrera(models.Model):

    nombre = models.CharField(max_length=100)
    descripcion = models.TextField()
    estMin = models.IntegerField()
    facultad = models.CharField(max_length=100)
    fechaApertura = models.DateTimeField(auto_now_add=True, auto_now=False)

    def __str__(self):
        return self.nombre

