from django.contrib import admin
from carreras.models import *
# Register your models here.

class AdminCarrera(admin.ModelAdmin):
    list_display = ["nombre", "facultad"]
    #form = RegModelForm
    list_filter = ["nombre", "facultad"]


admin.site.register(Carrera, AdminCarrera)
