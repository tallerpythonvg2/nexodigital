from django.db import models

# Create your models here.

class Facultad(models.Model):
    nombre = models.CharField(max_length=100)
    descripcion = models.TextField()
    ubicacion = models.TextField()
    minDepartamento = models.IntegerField()

    def __str__(self):
        return self.nombre
