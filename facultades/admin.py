from django.contrib import admin
from facultades.models import *
# Register your models here.

class AdminFacultad(admin.ModelAdmin):
    list_display = ["nombre", "ubicacion"]
    #form = RegModelForm
    list_filter = ["nombre", "ubicacion"]

admin.site.register(Facultad, AdminFacultad)

