from django.db import models

# Create your models here.

class Asignatura(models.Model):

    nombre = models.CharField(max_length=100)
    nomCarrera = models.CharField(max_length=100)
    semestre = models.IntegerField()
    fechaIncorporacion = models.DateTimeField(auto_now_add=True, auto_now=False)

    def __str__(self):
        return self.nombre