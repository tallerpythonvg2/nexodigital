from django.shortcuts import render
from .forms import AsignaturaForm
from .models import Asignatura
# Create your views here.

def inicio(request):
    form = AsignaturaForm(request.POST or None)
    if form.is_valid():
        form_data = form.cleaned_data
        nombre2 = form_data.get("nombre")
        email2 = form_data.get("carrera")
        objeto = Asignatura.objects.create(nombre=nombre2, email=email2)

    contexto ={
        "el_formulario": form,
    }
    return render(request, "RegAsignatura.html", contexto)