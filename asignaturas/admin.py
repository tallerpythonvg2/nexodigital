from django.contrib import admin
from asignaturas.models import *
# Register your models here.


class AdminAsignatura(admin.ModelAdmin):
    list_display = ["nombre", "semestre"]
    #form = RegModelForm
    list_filter = ["nombre", "semestre"]


admin.site.register(Asignatura, AdminAsignatura)
