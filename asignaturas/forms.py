from django import forms
from .models import Asignatura

class AsignaturaForm(forms.Form):
    nombre = forms.CharField(max_length=100)
    nomCarrera = forms.CharField(max_length=100)
    semestre = forms.IntegerField()

class AsignaturaModelForm(forms.ModelForm):
    class Meta:
        modelo = Asignatura
        campos = ["nombre", "nomCarrera"]